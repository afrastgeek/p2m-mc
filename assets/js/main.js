let member = [
  {nim: "1607648", nama: "Tia Herdiastuti", divisi: "Bendahara"},
  {nim: "1601067", nama: "Sofi Fauziah Rahmawati", divisi: "Bendahara"},

  {nim: "1606845", nama: "Nur Amruna Dini", divisi: "Sekretaris"},
  {nim: "1607262", nama: "Yola Nanda Sekar Prima", divisi: "Sekretaris"},

  {nim: "1505066", nama: "Bisma Wahyu Anaafie", divisi: "Dekorasi dan Dokumentasi"},
  {nim: "1608210", nama: "Doni Ramadan", divisi: "Dekorasi dan Dokumentasi"},
  {nim: "1500531", nama: "Herlina Andriani", divisi: "Dekorasi dan Dokumentasi"},
  {nim: "1701539", nama: "Muhammad Rizal Purnomo PP", divisi: "Dekorasi dan Dokumentasi"},
  {nim: "1700704", nama: "Reza Pahlevi", divisi: "Dekorasi dan Dokumentasi"},

  {nim: "1507553", nama: "Vivaldy Andhira", divisi: "Keamanan dan P3K"},
  {nim: "1507043", nama: "Raden Muhammad C. Aldian", divisi: "Keamanan dan P3K"},
  {nim: "1504748", nama: "Soffie Anastya Putri", divisi: "Keamanan dan P3K"},
  {nim: "1700795", nama: "Yayang Sri Marlina", divisi: "Keamanan dan P3K"},

  {nim: "1505064", nama: "Fatwa Muchammad Abdillah", divisi: "Acara Akademik"},
  {nim: "1500217", nama: "Teddy Koerniadi", divisi: "Acara Akademik"},
  {nim: "1604500", nama: "Amellya Mustikaningtyas R", divisi: "Acara Akademik"},
  {nim: "1600686", nama: "Hamdan Ilham Miftahulkhoir ", divisi: "Acara Akademik"},
  {nim: "1703734", nama: "Dela Adelia", divisi: "Acara Akademik"},
  {nim: "1703571", nama: "Dina Dwi Handayani", divisi: "Acara Akademik"},

  {nim: "1500655", nama: "Taufik Dzikri Pangestu", divisi: "Logistik dan Transportasi"},
  {nim: "1606394", nama: "Ilyas Bertram", divisi: "Logistik dan Transportasi"},
  {nim: "1604401", nama: "Bambang", divisi: "Logistik dan Transportasi"},
  {nim: "1607155", nama: "Diky Chairul Azwar", divisi: "Logistik dan Transportasi"},
  {nim: "1703204", nama: "Wibi", divisi: "Logistik dan Transportasi"},

  {nim: "1507062", nama: "Risa Ima Hafiqi", divisi: "Konsumsi"},
  {nim: "1700916", nama: "Altheara", divisi: "Konsumsi"},
  {nim: "1504919", nama: "Ziady Mubaraq", divisi: "Konsumsi"},

  {nim: "1601330", nama: "Renra Noviana", divisi: "Dana Usaha dan Sponsorship"},
  {nim: "1601918", nama: "Sofhia Nabilah", divisi: "Dana Usaha dan Sponsorship"},
  {nim: "1702033", nama: "Robi Naufal Kaosar", divisi: "Dana Usaha dan Sponsorship"},

  {nim: "1600659", nama: "Muhammad Furqan Nur Hakim", divisi: "Hubungan Masyarakat"},
  {nim: "1504414", nama: "Alfi Inayati", divisi: "Hubungan Masyarakat"},
  {nim: "1604125", nama: "Azizah Nurul K.", divisi: "Hubungan Masyarakat"},
  {nim: "1702307", nama: "Mohammad Zainul Alif Fathoni", divisi: "Hubungan Masyarakat"},

  {nim: "1504286", nama: "Burhanudin", divisi: "Publikasi"},
  {nim: "1701266", nama: "Rantty Gantini", divisi: "Publikasi"},
  {nim: "1600624", nama: "Taufiq Pratama Putra", divisi: "Publikasi"},

  {nim: "1606975", nama: "Galang Satria Prasadana", divisi: "Acara Sosial"},
  {nim: "1701575", nama: "Risky Mulia Kusumah Dewi", divisi: "Acara Sosial"},
  {nim: "1603719", nama: "Irfan Muhammad Faisal", divisi: "Acara Sosial"},
  {nim: "1700701", nama: "Bisma Pandara Suhan", divisi: "Acara Sosial"},
  {nim: "1501534", nama: "Maharani Pramesti", divisi: "Acara Sosial"},
  {nim: "1502010", nama: "Syifa MaylanSalam", divisi: "Acara Sosial"},

  {nim: "1607672", nama: "Fitri Ratna Dewi", divisi: "Acara Kerohanian"},
  {nim: "1602373", nama: "Gusmawan", divisi: "Acara Kerohanian"},
  {nim: "1606172", nama: "Asep Saepul Achmad", divisi: "Acara Kerohanian"},
  {nim: "1702642", nama: "Reni Nuryati", divisi: "Acara Kerohanian"},
  {nim: "1702715", nama: "Ahmad Fauzan", divisi: "Acara Kerohanian"},

  {nim: "1600571", nama: "Rizal Muharam", divisi: "Acara"},

  {nim: "1507506", nama: "M Ammar Fadhlur Rahman", divisi: "Steering Committee"},
  {nim: "1501443", nama: "Sabila Fauziyya", divisi: "Steering Committee"},
  {nim: "1501005", nama: "Rani Suprianti", divisi: "Steering Committee"},
  {nim: "1506479", nama: "Sifa Marcella F ", divisi: "Steering Committee"},

  {nim: "1505439", nama: "Muhamad Naufal Fazanadi", divisi: "Non Departemen"},
  {nim: "1405348", nama: "Irnanda Maulidya", divisi: "DPM"}
]

function isMember(arr, obj) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].nim === obj) {
      return true;
    }
  }
}

function getName(arr, obj) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].nim === obj) return arr[i].nama;
  }
}

function getDivision(arr, obj) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].nim === obj) return arr[i].divisi;
  }
}

$(document).ready(function() {

  function seeResults() {
    $('form').on('submit', function(e) { //use on if jQuery 1.7+
      e.preventDefault(); //prevent form from submitting
      var nim = document.getElementById("nim").value;

      if (nim == "") {
        $(".announcement--results--text").text("Mohon masukkan NIM anda.");
      } else if (isMember(member, nim)) {
        var name = getName(member, nim);
        var division = getDivision(member, nim);
        $(".announcement--results--text").text("Selamat, " + name + " (" + nim + "), anda bergabung bersama kami di Divisi " + division + ".");
      } else {
        $(".announcement--results--text").text("Hai, NIM " + nim + "! Mohon maaf anda belum dapat bergabung sebagai panitia P2M Kemakom.");
      }

    });
  }

  seeResults();

});
